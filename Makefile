.DEFAULT_GOAL := run

fmt:
	go fmt ./...
.PHONY:fmt
	
lint: fmt
	golint ./...
.PHONY:lint

vet: fmt
	go vet ./...
.PHONY:vet

build: vet
	go build emoji.go
.PHONY:build

strip: build
	strip emoji
.PHONY:strip

run: build
	./emoji $(ARGS)
.PHONY:build
