package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"

	"golang.org/x/net/html"
)

const URL string = "https://emojipedia.org/search/?q="

func main() {
	flag.Parse()
	args := flag.Args()

	if len(args) == 0 {
		usage()
	}

	var wg sync.WaitGroup

	// Async get and parse of emojipedia pages
	for _, word := range args {
		wg.Add(1)
		go func(word string) {
			defer wg.Done()

			site := get_page(URL + word)
			doc, _ := html.Parse(strings.NewReader(site))
			emojis := parse_emojis(doc)

			var str string
			for _, emoji := range emojis {
				str += emoji.String() + "\n"
			}
			fmt.Println(str)
		}(word)
		wg.Wait()
	}
}

func usage() {
	fmt.Println("Usage: emoji [words] ...")
	os.Exit(-1)
}

func get_page(url string) string {
	resp, err := http.Get(url)

	if err != nil {
		fmt.Println("no-internet-connection!")
		os.Exit(-1)
	}

	html, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println("invalid-response-body!")
		os.Exit(-1)
	}

	return string(html)

}

func get_all(doc *html.Node, tag string) []*html.Node {
	var body []*html.Node
	var crawler func(*html.Node)
	crawler = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == tag {
			body = append(body, node)
			return
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawler(child)
		}
	}
	crawler(doc)
	return body
}

func get_text(n *html.Node) string {
	var text string
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if c.Type == html.TextNode {
			text = c.Data
		}
	}
	return text
}

func parse_emojis(doc *html.Node) []Emoji {
	var emojis []Emoji
	ol := get_all(doc, "ol")
	as := get_all(ol[0], "a")

	for _, a := range as {
		spans := get_all(a, "span")
		for _, span := range spans {
			e := Emoji{get_text(span), get_text(a)}
			emojis = append(emojis, e)
		}
	}
	return emojis
}

type Emoji struct {
	emoji, desc string
}

func (e Emoji) String() string {
	return e.emoji + " " + e.desc
}
