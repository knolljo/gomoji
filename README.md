# Emoji finder

Simple script to parse emojipedia searchpage for searchterms.

## Installation

```console
make
```

## Usage

Outputs all found emojis for the searchterms.

```console
./gomoji [search term1] [search term2]...
```
